(fn S [...] `(minetest.translate "train_remote" ,...))
(fn Sf [...] `(minetest.formspec_escape (S ,...)))

(fn typep [obj typ]
  `(= (type ,obj) ,typ))

(fn toint [n]
  `(let [n# (tonumber ,n)]
     (if n# (math.floor n#) nil)))

(fn check-wagon-access [pname wagon]
  `(let [w# ,wagon]
     (advtrains.check_driving_couple_protection pname w#.owner w#.whitelist)))

(fn check-train-access [pname trainid]
  `(let [pn# ,pname tid# ,trainid]
     (if (not (minetest.get_player_by_name pn#))
         (values false (S "Player is not online"))
         (let [t# (. advtrains.trains tid#)]
           (if (not t#) (values false (S "No train with ID @1" tid#))
               (let [tps# t#.trainparts]
                 (local n# (length tps#))
                 (local wagons# advtrains.wagons)
                 (var i# 0)
                 (var contp# true)
                 (while (and (<= i# n#) contp#)
                   (let [w# (. wagons# (. tps# i#))]
                     (if (and w# (check-wagon-access pn# w#))
                         (set contp# false)
                         (set i# (+ 1 i#)))))
                 (not contp#)))))))

(fn leverof [train]
  `(let [t# ,train]
     (if (not t#) nil
         (and (= t#.velocity 0) (not t#.active_control)) 1
         t#.hud_lzb_effect_tmr 1
         (or t#.lever 3))))

(fn get-textlist [field max]
  `(let [et# (minetest.explode_textlist_event ,field)]
     (if (not et#) nil
         (and (or (= et#.type :CHG) (= et#.type :DCL))
              (>= et#.index 1) (<= et#.index ,max))
         et#.index
         nil)))

(fn has-scrollbar-event [field]
  `(let [et# (minetest.explode_scrollbar_event ,field)]
     (if (not et#) false
         (= et#.type :CHG))))

(fn get-scrollbar [field min max]
  `(let [et# (minetest.explode_scrollbar_event ,field)]
     (if (not et#) nil
         (and (= et#.type :CHG) (>= et#.value ,min) (<= et#.value ,max)) et#.value
         nil)))

{: S
 : Sf
 : typep
 : toint
 : check-wagon-access
 : check-train-access
 : leverof
 : get-textlist
 : has-scrollbar-event
 : get-scrollbar
 }
