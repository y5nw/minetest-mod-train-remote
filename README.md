# Advtrains remote

[![ContentDB](https://content.minetest.net/packages/yw05/train_remote/shields/title/)](https://content.minetest.net/packages/yw05/train_remote/)

**This mod is no longer maintained. Please use [Maverick2797's implementation](https://notabug.org/Maverick2797/advtrains_train_controller) instead.**

This mod allows you to remotely control a train in Minetest.

## Usage
You need to know the train ID of the mod. Use the `/train_remote` command to remotely control a train.

It is assumed that the user has some knowledge of the advtrains mod.

## Notes
* This mod is written in [Fennel Lisp](https://fennel-lang.org/). Do **not** file pull requests against `init.lua`.
* When filing a bug report, please give the commit hash (with `git rev-parse HEAD`) or the content of the `init.lua` file, as the default traceback function is not aware of the use of Fennel.
* Translations are not yet accepted as the mod is currently unstable and may change drastically.
* I have not tested this mod in a multiplayer environment. Use this mod with care on servers.
