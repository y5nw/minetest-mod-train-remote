(require-macros :helpers)

(macro ensure-string [obj]
  `(let [obj# ,obj]
     (if (typep obj# :string) obj#
         (typep obj# :number) (tostring obj#)
         (list 'tostring obj#))))

(macro maybe-concat [tbl delim]
  `(let [delim# ,delim tbl# ,tbl t# [] len# (length tbl#)]
     (var i# 1)
     (while (<= i# len#)
       (if (typep (. tbl# i#) :string)
           (let [start# i#]
             (while (typep (. tbl# (+ 1 i#)) :string)
               (set i# (+ 1 i#)))
             (table.insert t# (table.concat tbl# delim# start# i#)))
           (table.insert t# (. tbl# i#)))
       (set i# (+ 1 i#)))
     (if (not (. t# 1)) ""
         (not (. t# 2)) (. t# 1)
         (list 'table.concat t# delim#))))

(fn formspec [...]
  `(table.concat
    ,(icollect [_ i (ipairs [...])]
       (if (sequence? i)
           `(string.format
             "%s[%s]"
             ,(ensure-string (. i 1))
             ,(maybe-concat
               (icollect [j v (ipairs i)]
                 (if (<= j 1)
                     nil
                     (sequence? v)
                     (maybe-concat
                      (icollect [_ k (ipairs v)]
                        (ensure-string k))
                      ",")
                     (ensure-string v)))
               ";"))
           (ensure-string i)))
    " "))

{: formspec}
